# Zoologico :bear: :koala: 	:tiger: 	:monkey_face:
![Image text](https://www.sexadodeaves.com/vlog/wp-content/uploads/2019/08/educar-al-p%C3%BAblico-1024x374.jpg)

Los zoológicos son ahora centros de preservación y educación, donde se busca fomentar el respeto por la biodiversidad del planeta. Se busca entretener, pero no por esto se descuida la responsabilidad que implica mantener a una población de animales silvestres. El rol que cumplen es indispensable para la potencial preservación del medio ambiente y sus ecosistemas.
***
### :round_pushpin: Como funciona un zoologico :tiger:
La mayoría de los zoológicos, funcionan por medio de imitaciones del hábitat natural de los animales. Poseen grandes extensiones de terreno, en las cuales son liberados los animales y simulan ser los parajes del hábitat natural de cada animal respectivamente.
***
### Que abarca un zoologico
![Image text](https://estepais.com/wp-content/uploads/2022/05/estepais-AMBIENTE-ZOOS.png) 
### :triangular_flag_on_post: Exhibición de animales:
* El zoológico proporciona hábitats y entornos controlados que imitan los ambientes naturales de los animales, permitiendo a los visitantes observar una diversidad de especies de todo el mundo.
***
### :triangular_flag_on_post: Conservación:
* Muchos zoológicos participan en programas de cría en cautiverio y conservación de especies en peligro de extinción. Estos programas contribuyen a la preservación de especies amenazadas y, en algunos casos, a su reintroducción en la naturaleza.
***
### :triangular_flag_on_post: Educación: 
* Los zoológicos tienen un papel educativo crucial al proporcionar información sobre la biología, el comportamiento y la conservación de los animales. Suelen ofrecer programas educativos, visitas guiadas y charlas para estudiantes y el público en general.
***
### :triangular_flag_on_post: Investigación: 
* Algunos zoológicos llevan a cabo investigaciones científicas en áreas como el comportamiento animal, la genética, la fisiología y la reproducción. Estos estudios pueden contribuir al conocimiento de las especies y su conservación.
***
### :triangular_flag_on_post: Bienestar animal: 
* Los zoológicos se esfuerzan por proporcionar cuidados adecuados y enriquecimiento ambiental para el bienestar físico y mental de los animales en su custodia.
Planificación de hábitats: Los zoológicos diseñan hábitats que intentan replicar los entornos naturales de los animales, considerando factores como la vegetación, el suelo, el agua y las características climáticas.
***
### :triangular_flag_on_post: Administración:
* La gestión de un zoológico implica la planificación de las operaciones diarias, el cuidado de los animales, la seguridad de los visitantes, la administración financiera y el cumplimiento de las regulaciones y normativas.
***
### :triangular_flag_on_post: Interacción con el público: 
* Los zoológicos ofrecen una experiencia interactiva para los visitantes, incluyendo exhibiciones de animales, presentaciones en vivo, exhibiciones educativas y actividades prácticas.
***
### :triangular_flag_on_post: Financiamiento: 
* Los zoológicos obtienen ingresos a través de la venta de entradas, membresías, donaciones, patrocinios y tiendas de regalos. Estos fondos se destinan a mantener las instalaciones, cuidar de los animales y financiar programas de conservación y educación.
***
### :triangular_flag_on_post: Cambio en enfoque:
* A lo largo del tiempo, muchos zoológicos han evolucionado de ser simples lugares de exhibición a instituciones más centradas en la conservación, la investigación y la educación, contribuyendo de manera positiva a la preservación de la vida silvestre y al entendimiento de la relación entre los seres humanos y los animales.
***
### :triangular_flag_on_post: Obtencion etica de animales

1. Ya la obtención de animales no se realiza mediante la caza furtiva y la incursión forzada y dañina en ecosistemas delicados. Ahora los animales de los zoológicos se obtienen mediante la cría de animales dentro de la misma institución. Esto evita el deterioro de los ecosistemas naturales y ayuda a la compresión de los hábitats en los que viven las criaturas.

1. Otra forma de obtención es mediante la adopción o rescate de animales en cautiverio ilegal. Estos se obtienen al ser decomisados a personas que los obtienen por medio del mercado negro. Muchos de estos animales no pueden reintegrarse a su medio natural, por lo que son llevados a zoológicos. Allí pueden pasar el resto de sus vidas en circunstancias mucho más aceptables.
***
###  :triangular_flag_on_post: Mantenimiento adecuado de los animales
* Ya sean públicos o privados, en muchos países los zoológicos deben cumplir con ciertos parámetros de mantenimiento animal. Esto asegura que no se les dé un trato injusto y cruel a las criaturas que en él se encuentren. Los zoológicos que no garanticen estas condiciones pueden ser multados o vetados en ciertas organizaciones internacionales.
***
### :triangular_flag_on_post: Como es el proceso de adopcion de animales
* A menudo personas inconscientes extraen a animales silvestres de sus espacios naturales. Esto lo pueden hacer por cuestiones económicas o de lujo, o incluso por simple ignorancia del daño que causan. Cuando estos animales son descubiertos y debidamente incautados no pueden ser devueltos a sus hábitats debido a que están desacostumbrados. Hacer esto implicaría la muerte casi inmediata del animal.

* En estos casos los zoológicos están capacitados para recibir a estos animales y albergarlos para así proporcionarles una vida decente. Ya que no pueden ser reintroducidos es sus respectivos hábitats pueden quedar como una manera de educar a la población. En algunos casos incluso se pueden llegar a reeducar para ser devueltos sin problemas a sus espacios naturales.

***

Es importante destacar que el funcionamiento y enfoque de los zoológicos pueden variar según la ubicación geográfica, el tamaño, los recursos financieros y la misión específica de cada institución.
![Image text](https://assets-global.website-files.com/62685058d179ab5ae11b4e6a/62d01c9d55d53f7a2fb4fbae_grow%20zoo%20tours.jpg) 


## Diagrama de base de datos

<img src="/resources/img/Esquema_DB.jpeg">


## Definición del Proyecto:

El proyecto de registro de ingreso y salida de animales en un zoológico se desarrolló utilizando la versión v14.21.3 de Node.js como tecnología base. El proyecto de registro de ingreso y salida de animales en un zoológico es una solución diseñada para gestionar de manera eficiente y precisa la información relacionada con la llegada y partida de los animales en el entorno zoológico. Este sistema se basa en una base de datos MongoDB que almacena información crucial sobre los animales, su procedencia, supervisores involucrados, y detalles de adquisición. Además, permite un seguimiento detallado de la atención médica proporcionada por veterinarios, así como la información sobre hábitats y zonas dentro del zoológico.

El objetivo principal de este proyecto es proporcionar a los administradores del zoológico una herramienta robusta para el seguimiento y control de la población animal, asegurando el bienestar y la salud de los animales, facilitando la planificación de exposiciones y exhibiciones, y optimizando la gestión de recursos humanos y financieros.

En resumen, el proyecto de registro de ingreso y salida de animales en el zoológico busca mejorar la eficiencia operativa y el cuidado de los animales, al tiempo que brinda a los administradores la información necesaria para tomar decisiones informadas en beneficio de la institución y su misión de conservación y educación.
## Inicialización del proyecto. 

- Clonamos el proyecto en un directorio. 
- Entramos al directorio clonado

```bash
> git clone https://gitlab.com/stivendesarrolladorweb/zoologico_campus.git
> cd zoologico_campus
```

## Instala las dependencias 

- Ingresamos a la terminar estando dentro de la carpeta del proyecto y ejecutamos el siguiente comando.

```bash
npm install
```


## Crea el archivo de enviroments: 

- En la raiz del proyecto, crea un archivo ``.env``

- Configura las propiedades: Servidor de express, datos de mongodb atlas, key de token 

```text
MY_SERVER={"hostname":"", "port":}
ATLAS_USER=""
ATLAS_PASSWORD=""
ATLAS_DB=""
JWT_PRIVATE_KEY="HolaMundo"
```

##### IMPORTANTE
Para conectarse al servidor que nosotros tenemos montado, debes configurar la estructura del archivo .env de la siguiente manera.

```JS
MY_SERVER={"hostname":"127.0.0.1", "port":5015}
ATLAS_USER="eduardoma876"
ATLAS_PASSWORD="campus123"
ATLAS_DB="zoo_campus"
JWT_PRIVATE_KEY="HolaMundo"
 ```


- Para levantar el servidor debemos ejecutar el siguiente comando.

```js
npm run dev
```

- Para ejecutarlo y transpilar los archivos ejecuta el comando en la terminal, de la siguiente manera:

```bash
npm run tsc
```
## Proceso de Login
Se debe tener presente que el sistema cuenta con autentificacion por usuarios y que asi mismo estos usuarios tienen unos permisos los cuales le permitiran realizar ciertas consultas dentro del sistema, a continuacion se explicara como es el proceso correcto para generar un token y asi mismo poder realizar la consulta en un EndPoint

## Generar un token - Permiso de autentificacion dentro del sistema:

1. Primero debemos contar con un usuario y contraseña 

Ejemplo:
Usuario: admin456
Clave: claveAdmin789
Permisos: GET

Usuario: doctor_laura
Clave: doctorPass123
Permisos: POST

2. Nos dirigimos a la siguiente ruta:

http://midireccionip:puerto/login

Ejemplo:
http://127.10.10.1:5012/login

En el body debemos enviar esta info

{
  "usuario":"cuidador_juan",
  "password":"cuidadorPass789"
}

## Importante:
Recuerda que debes pasar un usuario y contraseña y asi mismo debe tener la misma estructura como fue compartida anteriormente.

Si la validacion es correcta el sistema nos arrojara un mensaje como el siguiente:

{
  "status": 200,
  "token": "eyJhbGciOiJIUzI1NiJ9.R0VU.WQ10eHeirqyd3iq3CC7ji0IAot8m9BHyYwtY16Cskpw"
}


Lo que nos interesa en estos momentos es el token, el cual lo utilizaremos para la consulta de EndPoints de tipo GET que es el permiso que tiene el usuario que se paso en el anterior ejemplo.

## Consultar EndPoint - Teniendo un token 

1. Teniendo nuestro token listo ahora lo que vamos hacer es copiar dicho token y pegarlo en el campo de Auth/Bearer el cual se encuentra ubicado en nuestros Headers 

2. Posteriormente ya podemos consultar los metodos a los cuales tenemos permiso 

GET - http://midireccionip:puerto/registro
- Ejemplo
http://127.10.10.1:5012/login

## Dato Importante 
cada usuario tiene distintos permisos con los que se pueden acceder a los metodos ya sea get o post
- Cada usuario tendra permisos para acceder a los endpoints a los cuales se le dio acceso.


## Puntos a tener en cuenta al usar un end point 
- Deben llevar la estructura correcta de lo contrario arrojara un error
- Los id con los que se enlazan las tablas deben existir 
- De no cumplir estos parametros la app arrojara un error como los siguientes 


## EndPoints ZoologicoCampus
- Generar Token a traves de usuario registrado en el sistema

(GET) http://127.10.10.1:5012/login
 ```json
{
    "usuario":"",
    "password":""
}

```
- Obtener todos los registros de la base de datos 
    -(GET) http://127.10.10.1:5012/registro 
- Obtener todos los animales 
    - (GET) http://127.10.10.1:5012/animales
- Crear un nuevo animal
    - (POST) http://127.10.10.1:5012/animales/new
 ```json
    {
    "animal_nombre":"Oso",
    "animal_edad": "3 Años",
    "animal_especie": "Oso Polar",
    "animal_idHabitat": 1,
    "animal_idZona": 102,
    "registro_date_created": "2023-10-12",
    "date_updated": "2023-10-15"
}
```
- Obtener todos los registros 
- Obtener un registro a traves de su id 
    - (GET) http://127.10.10.1:5012/registro/:idRegistro
Ejemplo
http://127.10.10.1:5012/registro/1
- Crear un nuevo registro 
    - (POST) http://127.10.10.1:5012/registro
```json 
{
    "registro_animalId": 5,
    "registro_fechaIngreso": "2023-08-01",
    "registro_procedencia": "Otro zoológico",
    "registro_observacion": "Animal en buen estado de salud.",
    "registro_idAdquisicion": 1,
    "registro_idSupervisor": 2,
    "registro_estado": "Egreso",
    "registro_date_created": "2023-08-01T08:00:00Z"
}
```

## Aclaracion importante 
Recuerde que los campos registro_animalId, registro_idAdquisicion, registro_idSupervisor de lo contrario no permitira hacer el registro porque dichos datos no existen en esas tablas.
## Siguientes Endpoints
- Obtener los animales que pertenezcan a la zona especificada por el usuario 
    - (GET) http://127.10.10.1:5012/animales/zona/:nombreZona
    - Ejemplo
http://127.10.10.1:5012/animales/zona/Zona Amazónica
- Obtener el dato de los animales que tengan una fecha de creacion en el sistema del año 2023 y que asi mismo el estado en su registro sea Egresado
    - (GET)  http://127.10.10.1:5012/animales/filtro/egresado
- Obtener el historial medico de un animal por su id 
    - (GET) http://127.10.10.1:5012/historial/:idHistorial
    - Ejemplo
http://127.10.10.1:5012/historial/1
- Obtener la cantidad de animales ingresados y egresados del zoologico
    - (GET) http://127.10.10.1:5012/registro/filtro/estado
- Obtener todos los veterinarios
    - (GET) http://127.10.10.1:5012/veterinarios
