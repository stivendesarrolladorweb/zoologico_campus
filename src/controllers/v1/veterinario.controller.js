import { conex } from "../../config/atlas.js";
import { ObjectId } from "mongodb";
import { appDTODataVeterinario } from "../../middlewares/veterinarioMiddleware.js";


let db = await conex();
let veterinario = db.collection("veterinario");
const getVeterinarioAll = async(req,res)=>{
    try {
        let result = await veterinario.aggregate([
            {
                $project: {
                    "id": 1,
                    "veterinario_nombre": "$nombre",
                    "veterinario_especialidad": "$especialidad",
                    "veterinario_habilidades": "$habilidades",
                    "veterinario_telefono": "$telefono"
                
                }
            }
        ]).toArray();
        return res.status(200).send(result);
    } catch (error) {
        res.status(500).send({message:error.stack});
    }
}
export{
    getVeterinarioAll,
}