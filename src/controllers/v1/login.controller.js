import dotenv from "dotenv";
import { conex } from "../../config/atlas.js";
import generateToken from "../../libs/generateToken.js"

dotenv.config("../../../")

const valdiateUser = async (user) =>{
    let db = await conex();
    let colec = db.collection("usuario");
    let result = await colec.findOne({usuario:user.usuario,password:user.password})
    if(!result) return false

    let token = await generateToken(result.permisos)
    return {status:200, token};
}

const login = async(req, res)=>{
    try{
        let usuario = await valdiateUser(req.body);
        
        if(usuario==false){
            res.status(401).send({message:"Usuario/Contraseña incorrecto"})
        }else{
            res.status(200).send(usuario)
        }
    }catch(e){
        res.status(200).send({message:e.message})
    }
}
export {login};