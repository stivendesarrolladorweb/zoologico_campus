import {conex} from "../../config/atlas.js";
import {validateId} from "../../middlewares/validateId.js"
import appIdAutomatico from "../../services/idAutomatico.js";
import appIdExist from "../../services/validacionIdExista.js";

const getRegistroAll = async(req,res) =>{
    try{
        let db = await conex();
        let registro = db.collection("registro");
        let result = await registro.find({},{projection:{
            _id:0,
            "registro_id":"$id",
            "registro_animalId":"$animalId",
            "regisro_fechaIngreso":"$fechaIngreso",
            "registro_procedencia":"$procedencia",
            "registro_observacion":"$observacion",
            "registro_idAquisicion":"$idAdquisicion",
            "registro_idSupervisor":"$idSupervisor",
            "registro_estado":"$estado"
        }}).toArray();
        res.send(result);
    }catch(e){
        res.status(400).send({messsage:`Error => ${e.message}`})
    }
}

const getRegistro = async(req,res) =>{
    try{
        let idRegistro = req.params.id; 
        let validacionId = validateId(idRegistro);
        if(validacionId === true){
        let db = await conex();
        let registro = db.collection("registro");
        let result = await registro.find({id:parseInt(idRegistro)},{projection:{
            _id:0,
            "registro_id":"$id",
            "registro_animalId":"$animalId",
            "regisro_fechaIngreso":"$fechaIngreso",
            "registro_procedencia":"$procedencia",
            "registro_observacion":"$observacion",
            "registro_idAquisicion":"$idAdquisicion",
            "registro_idSupervisor":"$idSupervisor",
            "registro_estado":"$estado"
        }}).toArray();
        //Object.keys(result).length =0 ? res.send({message:"No se encontraron registros"}): res.send(result) ;
        res.send(result)
        }else{
            res.send({message:validacionId})
        }
    }catch(e){
        res.status(400).send({messsage:`Error => ${e.message}`})
    }
}
const getRegistroEstado = async (req,res) =>{
    try{
        let db = await conex();
        let registro = db.collection("registro");
        let result = await registro.aggregate([
            {
                $group: {
                    _id:"$estado",
                    count:{$sum:1}
                }
            }
        ]).toArray();
        res.send(result)
    }catch(e){
        res.status(400).send({message:`Error => ${e.message}`})
    }
}
const addRegistro = async(req,res) =>{
    try{       
        let db = await conex();
        let registro = db.collection("registro");

        let validacionAnimalExist = await appIdExist("animal",req.body.animalId);
        let validacionAdquisicion = await appIdExist("adquisicion",req.body.idAdquisicion);
        let validacionSupervisor = await appIdExist("supervisor",req.body.idSupervisor);

        if(validacionAnimalExist){
            if(validacionAdquisicion){
                if(validacionSupervisor){
                    let query = {
                        "id": parseInt(await appIdAutomatico("registro")),
                        "animalId": parseInt(req.body.animalId),
                        "fechaIngreso": req.body.fechaIngreso,
                        "procedencia": req.body.procedencia,
                        "observacion": req.body.observacion,
                        "idAdquisicion": parseInt(req.body.idAdquisicion),
                        "idSupervisor": parseInt(req.body.idSupervisor),
                        "estado": req.body.estado,
                        "date_created": new Date().toISOString().split('T')[0]
                    }
                    await registro.insertOne(query);
                    res.send({message:`Registro agregado con exito`})
                }else{
                    res.status(404).send({message:"El campo registro_idSupervisor no existe en la base de datos, por favor digite un id que exista"})
                }
            }else{
                res.status(404).send({message:"El campo registro_idAdquisicion no existe en la base de datos, por favor digite un id que exista"})
            }
        }else{
            res.status(404).send({message:"El campo registro_animalId no existe en la base de datos, por favor digite un id que exista"})
        }
        

    }catch(e){
        res.status(400).send({messsage:`Error => ${e.message}`})
    }
}

const getAnimalEgresado = async(req,res)=>{
    try{       
        let db = await conex();
        let registro = db.collection("registro");
        let result =await registro.aggregate([
        {
            $match: {
                estado: "Egresado"
            }
        },
        {
            $project:{
                _id:0,
                "registro_id":"$id",
                "registro_animalId":"$animalId",
                "regisro_fechaIngreso":"$fechaIngreso",
                "registro_procedencia":"$procedencia",
                "registro_observacion":"$observacion",
                "registro_idAquisicion":"$idAdquisicion",
                "registro_idSupervisor":"$idSupervisor",
                "registro_estado":"$estado"
            }
        }
        ]).toArray();
        if(Object.keys(result).length == 0){
            res.status(200).send({messsage:`No se encontraron registros en el sistema.`})
        }else{
            res.status(200).send(result)
        }
    }catch(e){
        res.status(400).send({messsage:`Error => ${e.message}`})
    }
}

export {
    getRegistro,
    addRegistro,
    getRegistroAll,
    getRegistroEstado,
    getAnimalEgresado
}