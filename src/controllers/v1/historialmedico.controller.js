import { ObjectId } from "mongodb";
import {conex} from "../../config/atlas.js";
import { appDTODataHistorial } from "../../middlewares/historialMiddleware.js";
import { resolve } from "path";
import { validate } from "class-validator";
import { validateId } from "../../middlewares/validateId.js";


const getHistorialbyid= async(req, res) => {
    try{
        let idHistorial = req.params.id;
        let validacionId = validateId(idHistorial);
        if(validacionId === true){
            let db = await conex();
            let historial = db.collection("historial_medico");
            let result = await historial.find({id:parseInt(idHistorial)},{
                projection: {
                    "_id":0,
                    "id": "$id",
                    "historial_observacion": "$observacion",
                    "fecha_observacion": "$fecha",
                    "evento_historial": "$evento",
                    "historial_tratamiento": "$tratamiento",
                    "historial_dieta": "$dieta",
                    "historial_veterinario": "$idVeterinario",
                    "historial_animal": "$idAnimal"
                }
            }).toArray();
            res.send(result); 
        }else{
            res.send({message:validacionId})
        }
    }catch(e){
        res.status(400).send({message:`Error=> ${e.message}`})
    }
}


export {
    getHistorialbyid,
}