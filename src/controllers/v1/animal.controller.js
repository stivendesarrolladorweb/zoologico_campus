import { ObjectId } from "mongodb";
import {conex} from "../../config/atlas.js";
import { appDTODataAnimal } from "../../middlewares/animalMiddleware.js";
import {validateId} from "../../middlewares/validateId.js"
import appIdExist from "../../services/validacionIdExista.js";
import appIdAutomatico from "../../services/idAutomatico.js";


let db = await conex();
let animal = db.collection("animal");
const getAnimalAll=async(req,res)=>{
    try {
        let result = await animal.aggregate([
            {
                $project: {
                    "id": 1,
                    "animal_nombre": "$nombre",
                    "edad_animal": "$edad",
                    "especie_animal": "$especie",
                    "animal_habitat": "$idHabitat",
                    "Zona_animal": "$idZona",
                    "date_created": "$date_created",
                    "date_updated": "$date_updated",
                    "date_deleted": "$date_deleted",
                }
            }
        ]).toArray();
        return res.status(200).send(result); 
    } catch (error) {
        res.status(500).send({message:error.stack});
    }
}

const postAnimal = async (req,res)=>{
try{
    let validacionHabitatExist = await appIdExist("habitat", req.body.idHabitat)
    let validacionZonaExist = await appIdExist("zona", req.body.idZona)
    if (validacionHabitatExist && validacionZonaExist){
        let query = {
            "id": parseInt(await appIdAutomatico("animal")),
            "nombre": req.body.nombre,
            "edad": req.body.edad,
            "especie": req.body.especie,
            "idHabitat":  parseInt (req.body.idHabitat),
            "idZona": parseInt (req.body.idZona),
            "date_created":  req.body.date_created
          }
        await animal.insertOne(query);
    res.status(200).send({message:"Animal agregado con exito!!"});
}else{
    res.status(404).send({message: "El campo animal_idHabitat/animal_idZona no existe, por favor validar que los datos que estan ingresando existan en las demas colecciones."})
}
}catch (error){
    res.status(500).send({message:error.stack});
}
};

const getAnimalBebe = async (req,res)=>{
    let result = await animal.aggregate([
        {
          $lookup: {
            from: "registro",
            localField: "id",
            foreignField: "animalId",
            as: "registro"
          }
        },
        {
          $unwind: "$registro"
        },
        {
            $match:{
                "registro.date_created": { $regex: /^2023/ },
                "registro.estado":"Egreso"
            }
        },
        {
            $project:{
                "_id": 0,
                "animal_id": "$id",
                "animal_nombre": "$nombre",
                "animal_edad": "$edad",
                "animal_especie": "$especie",
                "animal_registro_fechaIngreso": "$registro.fechaIngreso",
                "animal_registro_procedencia": "$registro.procedencia",
                "animal_registro_observacion": "$registro.observacion",
                "animal_registro_estado": "$registro.estado"      
        }
        }
      ]).toArray();
      
    res.send(result);
}
const getAnimalFilterZona = async (req,res)=>{
    let nombreZona = req.params.zona;
    let result = await animal.aggregate([
            {
              $lookup: {
                from: "zona",
                localField: "idZona",
                foreignField: "id",
                as: "zonaInfo"
              }
            },
            {
              $match: {
                "zonaInfo.nombre": nombreZona
              }
            },
            {
                $project:{
                    "_id": 0,
                    "animal_nombre": "$nombre",
                    "animal_edad": "$edad",
                    "animal_especie": "$especie",
                    "animal_zona": { $arrayElemAt: ["$zonaInfo.nombre",0] }
                }
            }
          ]).toArray();
    if(Object.keys(result).length == 0){
        res.status(200).send({messsage:`No se encontraron registros en el sistema.`})
    }else{
        res.status(200).send(result)
    }
}
export {
    getAnimalAll,
    postAnimal,
    getAnimalBebe,
    getAnimalFilterZona
}