import {Router} from "express";
import routesVersioning from "express-routes-versioning";
import {getRegistroAllVersion,getRegistroVersion,getRegistroEstadoVersion, addRegistroVersion, getAnimalesEgresadosVersion} from "../support/v1.registro.js";
import {limitApi} from "../limits/limit.js";
import validateRegistro from "../middlewares/validateRegistro.js";
import { validateToken } from "../middlewares/validateToken.js";

const appRegistro = Router();
const version = routesVersioning();
appRegistro.use(limitApi())
appRegistro.get("/registro/:id",validateToken, version(getRegistroVersion));
appRegistro.get("/registro/filtro/estado",validateToken, version(getRegistroEstadoVersion));
appRegistro.get("/registro", validateToken, version(getRegistroAllVersion));
appRegistro.post("/registro",validateToken, validateRegistro, version(addRegistroVersion));
appRegistro.get("/registro/filtro/egresado",validateToken, version(getAnimalesEgresadosVersion));

export default appRegistro; 

