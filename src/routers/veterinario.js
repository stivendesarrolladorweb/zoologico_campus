import { Router } from "express";
import {limitApi} from "../limits/limit.js";
import routesVersioning from "express-routes-versioning";
import {getVeterinarioAllVersions} from "../support/v1.veterinario.js";
import { validateToken } from "../middlewares/validateToken.js";


const appVeterinario=Router();
const version = routesVersioning();
/* Endpoints para tabla animal*/

appVeterinario.get('/veterinarios',validateToken, limitApi(), version(getVeterinarioAllVersions));
export default appVeterinario;