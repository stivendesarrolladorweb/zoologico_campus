import { Router } from "express";
import {limitApi} from "../limits/limit.js";
import routesVersioning from "express-routes-versioning";
import {getHistorialbyidVersions} from "../support/v1.historial.js";
import { validateToken } from "../middlewares/validateToken.js";


const appHistorial=Router();
const version = routesVersioning();
/* Endpoints para tabla animal*/

appHistorial.get('/historial/:id',validateToken, limitApi(), version(getHistorialbyidVersions));
export default appHistorial;