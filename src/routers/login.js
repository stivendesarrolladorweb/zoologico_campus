import { Router } from "express";
import {limitApi} from "../limits/limit.js";
import routesVersioning from "express-routes-versioning";
import {getLoginUser} from "../support/v1.login.js";


const appLogin=Router();
const version = routesVersioning();

appLogin.get('/login', limitApi(), version(getLoginUser));

export default appLogin;