import { Router } from "express";
import {limitApi} from "../limits/limit.js";
import routesVersioning from "express-routes-versioning";
import {getAnimalAllVersions, postAnimalVersions, getAnimalZona,getAnimalesRecientes} from "../support/v1.animal.js";
import { appDTODataAnimal } from "../middlewares/animalMiddleware.js";
import { validateToken } from "../middlewares/validateToken.js";

const appAnimal=Router();
const version = routesVersioning();
/* Endpoints para tabla animal*/

appAnimal.use(limitApi());
appAnimal.get('/animales', validateToken, version(getAnimalAllVersions));
appAnimal.get('/animales/filtro/egresado', validateToken, version(getAnimalesRecientes));
appAnimal.get('/animales/zona/:zona', validateToken, version(getAnimalZona))
appAnimal.post('/animales/new', validateToken, appDTODataAnimal,version(postAnimalVersions));
export default appAnimal;

