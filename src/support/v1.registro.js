import {getRegistroAll, addRegistro,getRegistro,getRegistroEstado,getAnimalEgresado} from "../controllers/v1/registro.controller.js";

let getRegistroAllVersion = {
    "1.0.0": getRegistroAll,
}

let getRegistroVersion ={
    "1.0.0": getRegistro
}
let getRegistroEstadoVersion={
    "1.0.0": getRegistroEstado,
}
let addRegistroVersion = {
    "1.0.0": addRegistro
}

let getAnimalesEgresadosVersion = {
    "1.0.0": getAnimalEgresado
}

export {
    getRegistroAllVersion,
    getRegistroVersion,
    getRegistroEstadoVersion,
    addRegistroVersion,
    getAnimalesEgresadosVersion
}
