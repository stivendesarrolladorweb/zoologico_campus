import { getAnimalAll,postAnimal, getAnimalBebe, getAnimalFilterZona } from "../controllers/v1/animal.controller.js";

let getAnimalAllVersions = {
"1.0.0": getAnimalAll,
}

let postAnimalVersions = { 
    "1.0.0": postAnimal
}

let getAnimalesRecientes = { 
    "1.0.0": getAnimalBebe
}

let getAnimalZona = { 
    "1.0.0": getAnimalFilterZona
}


export{
    getAnimalAllVersions,
    postAnimalVersions,
    getAnimalesRecientes,
    getAnimalZona
}