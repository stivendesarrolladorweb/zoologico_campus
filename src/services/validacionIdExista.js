import { conex } from "../config/atlas.js";

const appIdExist = async(tabla, idValidar)=>{
    let db = await conex();
    let respuesta = db.collection(tabla);
    let result = await respuesta.find({id:idValidar}).toArray();
    let res=false;
    result.length==0?res=false:res=true;
    return res;
}

export default appIdExist;