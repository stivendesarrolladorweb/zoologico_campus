import { conex } from "../config/atlas.js";

const appIdAutomatico = async(tabla)=>{
    let db =await conex();
    let collection = db.collection(tabla)
    let result = await collection
    .find({},{projection:{_id:0,id:1}})
    .sort({ id: -1 })
    .limit(1)
    .toArray();
    return result.length===0?1:result[0].id+1;
}


export default appIdAutomatico;