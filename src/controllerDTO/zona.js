var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
class Zona {
    constructor(user) {
        Object.assign(this, user);
        this.id = 0,
            this.nombre = "",
            this.ubicacion = "";
    }
}
__decorate([
    Expose({ name: "zonaid" }),
    IsDefined({ message: () => { throw "Parametro zonaid requerido"; } }),
    IsInt({ message: () => { throw "Parametro zonaid debe ser numerico "; } }),
    __metadata("design:type", Number)
], Zona.prototype, "id", void 0);
__decorate([
    Expose({ name: "nombrezona" }),
    IsDefined({ message: () => { throw "Parametro nombrezona es requerido"; } }),
    IsString({ message: () => { throw "Parametro nombrezona debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], Zona.prototype, "nombre", void 0);
__decorate([
    Expose({ name: "ubicacionzona" }),
    IsDefined({ message: () => { throw "Parametro ubicacionzona es requerido"; } }),
    IsString({ message: () => { throw "Parametro ubicacionzona debe ser de tipo string"; } }),
    __metadata("design:type", String)
], Zona.prototype, "ubicacion", void 0);
export default Zona;
