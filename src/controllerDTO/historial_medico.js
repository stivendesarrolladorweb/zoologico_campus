var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
class dtoHistorial {
    constructor(user) {
        Object.assign(this, user);
        this.id = 0,
            this.observacion = "",
            this.fecha = "",
            this.evento = "",
            this.tratamiento = "",
            this.dieta = "",
            this.idVeterinario = 0,
            this.idAnimal = 0;
    }
}
__decorate([
    Expose({ name: "historialid" }),
    IsDefined({ message: () => { throw "Parametro historialid requerido"; } }),
    IsInt({ message: () => { throw "Parametro historialid debe ser numerico "; } }),
    __metadata("design:type", Number)
], dtoHistorial.prototype, "id", void 0);
__decorate([
    Expose({ name: "observacionhistorial" }),
    IsDefined({ message: () => { throw "Parametro observacionhistorial es requerido"; } }),
    IsString({ message: () => { throw "Parametro observacionhistorial debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoHistorial.prototype, "observacion", void 0);
__decorate([
    Expose({ name: "fechahistorial" }),
    IsDefined({ message: () => { throw "Parametro fechahistorial es requerido"; } }),
    IsString({ message: () => { throw "Parametro fechahistorial debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoHistorial.prototype, "fecha", void 0);
__decorate([
    Expose({ name: "eventohistorial" }),
    IsDefined({ message: () => { throw "Parametro eventohistorial es requerido"; } }),
    IsString({ message: () => { throw "Parametro eventohistorial debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoHistorial.prototype, "evento", void 0);
__decorate([
    Expose({ name: "tratamientohistorial" }),
    IsDefined({ message: () => { throw "Parametro tratamientohistorial es requerido"; } }),
    IsString({ message: () => { throw "Parametro tratamientohistorial debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoHistorial.prototype, "tratamiento", void 0);
__decorate([
    Expose({ name: "dietahistorial" }),
    IsDefined({ message: () => { throw "Parametro dietahistorial es requerido"; } }),
    IsString({ message: () => { throw "Parametro dietahistorial debe ser de tipo string"; } }),
    __metadata("design:type", String)
], dtoHistorial.prototype, "dieta", void 0);
__decorate([
    Expose({ name: "idVeterinariohis" }),
    IsDefined({ message: () => { throw "Parametro idVeterinariohis es requerido"; } }),
    IsString({ message: () => { throw "Parametro idVeterinariohis debe ser de tipo string"; } }),
    __metadata("design:type", Number)
], dtoHistorial.prototype, "idVeterinario", void 0);
__decorate([
    Expose({ name: "idAnimalhis" }),
    IsDefined({ message: () => { throw "Parametro idAnimalhis es requerido"; } }),
    IsString({ message: () => { throw "Parametro idAnimalhis debe ser de tipo string"; } }),
    __metadata("design:type", Number)
], dtoHistorial.prototype, "idAnimal", void 0);
export default dtoHistorial;
