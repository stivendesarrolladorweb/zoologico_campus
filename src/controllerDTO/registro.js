var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
export class dtoRegistro {
    constructor(data) {
        Object.assign(this, data);
        this.animalId = 0;
        this.fechaIngreso = "";
        this.procedencia = "";
        this.observacion = "";
        this.idAdquisicion = 0;
        this.idSupervisor = 0;
        this.estado = "";
        this.date_created = "2000-00-00";
        this.date_updated = "2000-00-00";
        this.date_deleted = "2000-00-00";
    }
}
__decorate([
    Expose({ name: 'registro_animalId' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_animalId es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo registro_animalId debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoRegistro.prototype, "animalId", void 0);
__decorate([
    Expose({ name: 'registro_fechaIngreso' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_fechaIngreso es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo registro_fechaIngreso debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoRegistro.prototype, "fechaIngreso", void 0);
__decorate([
    Expose({ name: 'registro_procedencia' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_procedencia es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo registro_procedencia debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoRegistro.prototype, "procedencia", void 0);
__decorate([
    Expose({ name: 'registro_observacion' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_observacion es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo registro_observacion debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoRegistro.prototype, "observacion", void 0);
__decorate([
    Expose({ name: 'registro_idAdquisicion' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_idAdquisicion es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo registro_idAdquisicion debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoRegistro.prototype, "idAdquisicion", void 0);
__decorate([
    Expose({ name: 'registro_idSupervisor' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_idSupervisor es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo registro_idSupervisor debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoRegistro.prototype, "idSupervisor", void 0);
__decorate([
    Expose({ name: 'registro_estado' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_estado es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo registro_estado debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoRegistro.prototype, "estado", void 0);
__decorate([
    Expose({ name: 'registro_date_created' }),
    IsString({ message: () => { throw { status: 422, message: `El campo registro_date_created debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoRegistro.prototype, "date_created", void 0);
__decorate([
    Expose({ name: 'registro_date_updated' })
    //@IsString({message: ()=>{ throw {status: 422, message: `El campo registro_date_updated debe ser de tipo string`}}})
    ,
    __metadata("design:type", String)
], dtoRegistro.prototype, "date_updated", void 0);
__decorate([
    Expose({ name: 'registro_date_deleted' })
    //@IsString({message: ()=>{ throw {status: 422, message: `El campo registro_date_deleted debe ser de tipo string`}}})
    ,
    __metadata("design:type", String)
], dtoRegistro.prototype, "date_deleted", void 0);
