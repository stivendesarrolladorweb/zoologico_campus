var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
class Habitat {
    constructor(user) {
        Object.assign(this, user);
        this.id = 0,
            this.nombre = "",
            this.clima = "";
    }
}
__decorate([
    Expose({ name: "habitatid" }),
    IsDefined({ message: () => { throw "Parametro habitatid requerido"; } }),
    IsInt({ message: () => { throw "Parametro habitatid debe ser numerico "; } }),
    __metadata("design:type", Number)
], Habitat.prototype, "id", void 0);
__decorate([
    Expose({ name: "nombrehabitat" }),
    IsDefined({ message: () => { throw "Parametro nombrehabitat es requerido"; } }),
    IsString({ message: () => { throw "Parametro nombrehabitat debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], Habitat.prototype, "nombre", void 0);
__decorate([
    Expose({ name: "clima" }),
    IsDefined({ message: () => { throw "Parametro clima es requerido"; } }),
    IsString({ message: () => { throw "Parametro clima debe ser de tipo string"; } }),
    __metadata("design:type", String)
], Habitat.prototype, "clima", void 0);
export default Habitat;
