var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
export class dtoAdquisicion {
    constructor(data) {
        Object.assign(this, data);
        this.id = 0;
        this.nombre = "";
        this.comentarios = "";
    }
}
__decorate([
    Expose({ name: 'adquisicion_id' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo adquisicion_id es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo adquisicion_id debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoAdquisicion.prototype, "id", void 0);
__decorate([
    Expose({ name: 'adquisicion_nombre' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo adquisicion_nombre es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo adquisicion_nombre debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoAdquisicion.prototype, "nombre", void 0);
__decorate([
    Expose({ name: 'adquisicion_comentarios' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo adquisicion_comentarios es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo adquisicion_comentarios debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoAdquisicion.prototype, "comentarios", void 0);
