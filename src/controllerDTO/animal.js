var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
export class dtoAnimal {
    constructor(data) {
        Object.assign(this, data);
        this.nombre = "";
        this.edad = "";
        this.especie = "";
        this.idHabitat = 0;
        this.idZona = 0;
        this.date_created = "";
    }
}
__decorate([
    Expose({ name: 'animal_nombre' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo animal_nombre es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo animal_nombre debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoAnimal.prototype, "nombre", void 0);
__decorate([
    Expose({ name: 'animal_edad' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo animal_edad es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo animal_edad debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoAnimal.prototype, "edad", void 0);
__decorate([
    Expose({ name: 'animal_especie' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo animal_especie es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo animal_especie debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoAnimal.prototype, "especie", void 0);
__decorate([
    Expose({ name: 'animal_idHabitat' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo animal_idHabitat es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo animal_idHabitat debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoAnimal.prototype, "idHabitat", void 0);
__decorate([
    Expose({ name: 'animal_idZona' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo animal_idZona es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo animal_idZona debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoAnimal.prototype, "idZona", void 0);
__decorate([
    Expose({ name: 'registro_date_created' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo registro_date_created es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo registro_date_created debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoAnimal.prototype, "date_created", void 0);
