var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
export class dtoSupervisor {
    constructor(data) {
        Object.assign(this, data);
        this.id = 0;
        this.nombre = "";
        this.cargo = "";
        this.telefono = "";
        this.habilidades = "";
    }
}
__decorate([
    Expose({ name: 'supervisor_id' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo supervisor_id es obligatorio` }; } }),
    IsInt({ message: () => { throw { status: 422, message: `El campo supervisor_id debe ser de tipo int` }; } }),
    __metadata("design:type", Number)
], dtoSupervisor.prototype, "id", void 0);
__decorate([
    Expose({ name: 'supervisor_nombre' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo supervisor_nombre es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo supervisor_nombre debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoSupervisor.prototype, "nombre", void 0);
__decorate([
    Expose({ name: 'supervisor_cargo' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo supervisor_cargo es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo supervisor_cargo debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoSupervisor.prototype, "cargo", void 0);
__decorate([
    Expose({ name: 'supervisor_telefono' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo supervisor_telefono es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo supervisor_telefono debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoSupervisor.prototype, "telefono", void 0);
__decorate([
    Expose({ name: 'supervisor_habilidades' }),
    IsDefined({ message: () => { throw { status: 422, message: `El campo supervisor_habilidades es obligatorio` }; } }),
    IsString({ message: () => { throw { status: 422, message: `El campo supervisor_habilidades debe ser de tipo string` }; } }),
    __metadata("design:type", String)
], dtoSupervisor.prototype, "habilidades", void 0);
