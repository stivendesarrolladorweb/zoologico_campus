var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Expose } from "class-transformer";
import { IsDefined, IsString, IsInt } from "class-validator";
class dtoVeterinario {
    constructor(user) {
        Object.assign(this, user);
        this.id = 0,
            this.nombre = "",
            this.especialidad = "",
            this.habilidades = "",
            this.telefono = "";
    }
}
__decorate([
    Expose({ name: "veterinarioid" }),
    IsDefined({ message: () => { throw "Parametro veterinarioid requerido"; } }),
    IsInt({ message: () => { throw "Parametro veterinarioid debe ser numerico "; } }),
    __metadata("design:type", Number)
], dtoVeterinario.prototype, "id", void 0);
__decorate([
    Expose({ name: "nombreveterinario" }),
    IsDefined({ message: () => { throw "Parametro nombreveterinario es requerido"; } }),
    IsString({ message: () => { throw "Parametro nombreveterinario debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoVeterinario.prototype, "nombre", void 0);
__decorate([
    Expose({ name: "especialidadveterinario" }),
    IsDefined({ message: () => { throw "Parametro especialidadveterinario es requerido"; } }),
    IsString({ message: () => { throw "Parametro especialidadveterinario debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoVeterinario.prototype, "especialidad", void 0);
__decorate([
    Expose({ name: "habilidadesveterinario" }),
    IsDefined({ message: () => { throw "Parametro habilidadesveterinario es requerido"; } }),
    IsString({ message: () => { throw "Parametro habilidadesveterinario debe ser un dato de tipo string"; } }),
    __metadata("design:type", String)
], dtoVeterinario.prototype, "habilidades", void 0);
__decorate([
    Expose({ name: "telefono" }),
    IsDefined({ message: () => { throw "Parametro telefono es requerido"; } }),
    IsString({ message: () => { throw "Parametro telefono debe ser de tipo string"; } }),
    __metadata("design:type", String)
], dtoVeterinario.prototype, "telefono", void 0);
export default dtoVeterinario;
