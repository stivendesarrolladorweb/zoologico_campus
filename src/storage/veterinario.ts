import { Expose, Transform } from "class-transformer"
import { IsDefined, IsString, IsInt } from "class-validator"

class dtoVeterinario {
  @Expose({ name: "veterinarioid" })
  @IsDefined({ message: () => { throw "Parametro veterinarioid requerido" } })
  @IsInt({ message: ()=>{throw "Parametro veterinarioid debe ser numerico "}})
  id: number

  @Expose({ name: "nombreveterinario" })
  @IsDefined({ message: () => { throw "Parametro nombreveterinario es requerido" } })
  @IsString({ message: () => { throw "Parametro nombreveterinario debe ser un dato de tipo string"}})
  nombre: string

  @Expose({ name: "especialidadveterinario" })
  @IsDefined({ message: () => { throw "Parametro especialidadveterinario es requerido" } })
  @IsString({ message: () => { throw "Parametro especialidadveterinario debe ser un dato de tipo string"}})
  especialidad: string

  @Expose({ name: "habilidadesveterinario" })
  @IsDefined({ message: () => { throw "Parametro habilidadesveterinario es requerido" } })
  @IsString({ message: () => { throw "Parametro habilidadesveterinario debe ser un dato de tipo string"}})
  habilidades: string

  @Expose({ name: "telefono" })
  @IsDefined({ message: () => { throw "Parametro telefono es requerido" } })
  @IsString({message: () => { throw "Parametro telefono debe ser de tipo string"}})
  telefono: string


  constructor(user: Partial<dtoVeterinario>) {
    Object.assign(this, user);
    this.id = 0,
    this.nombre = "",
    this.especialidad = "",
    this.habilidades = "",
    this.telefono = ""
  }
}

export default dtoVeterinario;