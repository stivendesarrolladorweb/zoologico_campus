import { Expose, Transform } from "class-transformer"
import { IsDefined, IsString, IsInt } from "class-validator"

class Habitat {
  @Expose({ name: "habitatid" })
  @IsDefined({ message: () => { throw "Parametro habitatid requerido" } })
  @IsInt({ message: ()=>{throw "Parametro habitatid debe ser numerico "}})
  id: number

  @Expose({ name: "nombrehabitat" })
  @IsDefined({ message: () => { throw "Parametro nombrehabitat es requerido" } })
  @IsString({ message: () => { throw "Parametro nombrehabitat debe ser un dato de tipo string"}})
  nombre: string

  @Expose({ name: "clima" })
  @IsDefined({ message: () => { throw "Parametro clima es requerido" } })
  @IsString({message: () => { throw "Parametro clima debe ser de tipo string"}})
  clima: string


  constructor(user: Partial<Habitat>) {
    Object.assign(this, user);
    this.id = 0,
    this.nombre = "",
    this.clima = ""
  }
}

export default Habitat;