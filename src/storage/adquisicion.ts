import { Expose} from "class-transformer";
import { IsDefined,IsString,IsInt } from "class-validator";

export class dtoAdquisicion{
    @Expose({name:'adquisicion_id'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo adquisicion_id es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo adquisicion_id debe ser de tipo int`}}})
    id:number;

    @Expose({name:'adquisicion_nombre'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo adquisicion_nombre es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo adquisicion_nombre debe ser de tipo string`}}})
    nombre:string;

    @Expose({name:'adquisicion_comentarios'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo adquisicion_comentarios es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo adquisicion_comentarios debe ser de tipo string`}}})
    comentarios:string;

    constructor(data: Partial<dtoAdquisicion>){
        Object.assign(this, data);
        this.id = 0;
        this.nombre="";
        this.comentarios="";
    }
}