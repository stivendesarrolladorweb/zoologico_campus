import { Expose, Transform } from "class-transformer"
import { IsDefined, IsString, IsInt } from "class-validator"

class dtoHistorial {
  @Expose({ name: "historialid" })
  @IsDefined({ message: () => { throw "Parametro historialid requerido" } })
  @IsInt({ message: ()=>{throw "Parametro historialid debe ser numerico "}})
  id: number

  @Expose({ name: "observacionhistorial" })
  @IsDefined({ message: () => { throw "Parametro observacionhistorial es requerido" } })
  @IsString({ message: () => { throw "Parametro observacionhistorial debe ser un dato de tipo string"}})
  observacion: string

  @Expose({ name: "fechahistorial" })
  @IsDefined({ message: () => { throw "Parametro fechahistorial es requerido" } })
  @IsString({ message: () => { throw "Parametro fechahistorial debe ser un dato de tipo string"}})
  fecha: string

  @Expose({ name: "eventohistorial" })
  @IsDefined({ message: () => { throw "Parametro eventohistorial es requerido" } })
  @IsString({ message: () => { throw "Parametro eventohistorial debe ser un dato de tipo string"}})
  evento: string

  @Expose({ name: "tratamientohistorial" })
  @IsDefined({ message: () => { throw "Parametro tratamientohistorial es requerido" } })
  @IsString({ message: () => { throw "Parametro tratamientohistorial debe ser un dato de tipo string"}})
  tratamiento: string

  @Expose({ name: "dietahistorial" })
  @IsDefined({ message: () => { throw "Parametro dietahistorial es requerido" } })
  @IsString({message: () => { throw "Parametro dietahistorial debe ser de tipo string"}})
  dieta: string

  @Expose({ name: "idVeterinariohis" })
  @IsDefined({ message: () => { throw "Parametro idVeterinariohis es requerido" } })
  @IsString({message: () => { throw "Parametro idVeterinariohis debe ser de tipo string"}})
  idVeterinario: number

  @Expose({ name: "idAnimalhis" })
  @IsDefined({ message: () => { throw "Parametro idAnimalhis es requerido" } })
  @IsString({message: () => { throw "Parametro idAnimalhis debe ser de tipo string"}})
  idAnimal: number

  constructor(user: Partial<dtoHistorial>) {
    Object.assign(this, user);
    this.id = 0,
    this.observacion = "",
    this.fecha = "",
    this.evento = "",
    this.tratamiento = "",
    this.dieta = "",
    this.idVeterinario = 0,
    this.idAnimal = 0
  }
}

export default dtoHistorial;