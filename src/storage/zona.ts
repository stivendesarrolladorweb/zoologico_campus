import { Expose, Transform } from "class-transformer"
import { IsDefined, IsString, IsInt } from "class-validator"

class Zona {
  @Expose({ name: "zonaid" })
  @IsDefined({ message: () => { throw "Parametro zonaid requerido" } })
  @IsInt({ message: ()=>{throw "Parametro zonaid debe ser numerico "}})
  id: number

  @Expose({ name: "nombrezona" })
  @IsDefined({ message: () => { throw "Parametro nombrezona es requerido" } })
  @IsString({ message: () => { throw "Parametro nombrezona debe ser un dato de tipo string"}})
  nombre: string

  @Expose({ name: "ubicacionzona" })
  @IsDefined({ message: () => { throw "Parametro ubicacionzona es requerido" } })
  @IsString({message: () => { throw "Parametro ubicacionzona debe ser de tipo string"}})
  ubicacion: string


  constructor(user: Partial<Zona>) {
    Object.assign(this, user);
    this.id = 0,
    this.nombre = "",
    this.ubicacion = ""
  }
}

export default Zona;