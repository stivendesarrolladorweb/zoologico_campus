import { Expose} from "class-transformer";
import { IsDefined,IsString,IsInt } from "class-validator";

export class dtoSupervisor{
    @Expose({name:'supervisor_id'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo supervisor_id es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo supervisor_id debe ser de tipo int`}}})
    id:number;

    @Expose({name:'supervisor_nombre'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo supervisor_nombre es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo supervisor_nombre debe ser de tipo string`}}})
    nombre:string;

    @Expose({name:'supervisor_cargo'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo supervisor_cargo es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo supervisor_cargo debe ser de tipo string`}}})
    cargo:string;

    @Expose({name:'supervisor_telefono'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo supervisor_telefono es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo supervisor_telefono debe ser de tipo string`}}})
    telefono:string;

    @Expose({name:'supervisor_habilidades'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo supervisor_habilidades es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo supervisor_habilidades debe ser de tipo string`}}})
    habilidades:string;


    constructor(data: Partial<dtoSupervisor>){
        Object.assign(this, data);
        this.id = 0;
        this.nombre="";
        this.cargo="";
        this.telefono="";
        this.habilidades="";
    }
}