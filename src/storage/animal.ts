import { Expose} from "class-transformer";
import { IsDefined,IsString,IsInt } from "class-validator";

export class dtoAnimal{

    @Expose({name:'animal_nombre'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo animal_nombre es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo animal_nombre debe ser de tipo string`}}})
    nombre:string;

    @Expose({name:'animal_edad'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo animal_edad es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo animal_edad debe ser de tipo string`}}})
    edad:string;

    @Expose({name:'animal_especie'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo animal_especie es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo animal_especie debe ser de tipo string`}}})
    especie:string;

    @Expose({name:'animal_idHabitat'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo animal_idHabitat es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo animal_idHabitat debe ser de tipo int`}}})
    idHabitat:number;

    @Expose({name:'animal_idZona'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo animal_idZona es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo animal_idZona debe ser de tipo int`}}})
    idZona:number;

    @Expose({name:'registro_date_created'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_date_created es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo registro_date_created debe ser de tipo string`}}})
    date_created:string;

    constructor(data: Partial<dtoAnimal>){
        Object.assign(this, data);
        this.nombre="";
        this.edad="";
        this.especie="";
        this.idHabitat=0;
        this.idZona=0;
        this.date_created="";
    }
}