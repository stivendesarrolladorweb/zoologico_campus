import { Expose} from "class-transformer";
import { IsDefined,IsString,IsInt } from "class-validator";

export class dtoUsuario{
    @Expose({name:'usuario_id'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo usuario_id es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo usuario_id debe ser de tipo int`}}})
    id:number;

    @Expose({name:'usuario_usuario'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo usuario_usuario es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo usuario_usuario debe ser de tipo string`}}})
    usuario:string;

    @Expose({name:'usuario_password'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo usuario_password es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo usuario_password debe ser de tipo string`}}})
    password:string;

    @Expose({name:'usuario_permisos'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo usuario_permisos es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo usuario_permisos debe ser de tipo string`}}})
    permisos:string;

    constructor(data: Partial<dtoUsuario>){
        Object.assign(this, data);
        this.id = 0;
        this.usuario="";
        this.password="";
        this.permisos="";
    }
}