import { Expose} from "class-transformer";
import { IsDefined,IsString,IsInt } from "class-validator";

export class dtoRegistro{
    @Expose({name:'registro_animalId'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_animalId es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo registro_animalId debe ser de tipo int`}}})
    animalId:number;

    @Expose({name:'registro_fechaIngreso'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_fechaIngreso es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo registro_fechaIngreso debe ser de tipo string`}}})
    fechaIngreso:string;

    @Expose({name:'registro_procedencia'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_procedencia es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo registro_procedencia debe ser de tipo string`}}})
    procedencia:string;

    @Expose({name:'registro_observacion'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_observacion es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo registro_observacion debe ser de tipo string`}}})
    observacion:string;

    @Expose({name:'registro_idAdquisicion'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_idAdquisicion es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo registro_idAdquisicion debe ser de tipo int`}}})
    idAdquisicion:number;

    @Expose({name:'registro_idSupervisor'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_idSupervisor es obligatorio`}}})
    @IsInt({message: ()=>{ throw {status: 422, message: `El campo registro_idSupervisor debe ser de tipo int`}}})
    idSupervisor:number;

    @Expose({name:'registro_estado'})
    @IsDefined({message: ()=>{ throw {status: 422, message: `El campo registro_estado es obligatorio`}}})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo registro_estado debe ser de tipo string`}}})
    estado:string;

    @Expose({name:'registro_date_created'})
    @IsString({message: ()=>{ throw {status: 422, message: `El campo registro_date_created debe ser de tipo string`}}})
    date_created:string;

    @Expose({name:'registro_date_updated'})
    //@IsString({message: ()=>{ throw {status: 422, message: `El campo registro_date_updated debe ser de tipo string`}}})
    date_updated?:string;

    @Expose({name:'registro_date_deleted'})
    //@IsString({message: ()=>{ throw {status: 422, message: `El campo registro_date_deleted debe ser de tipo string`}}})
    date_deleted?:string;

    constructor(data: Partial<dtoRegistro>){
        Object.assign(this, data);
        this.animalId=0;
        this.fechaIngreso="";
        this.procedencia="";
        this.observacion="";
        this.idAdquisicion=0;
        this.idSupervisor=0;
        this.estado="";
        this.date_created="2000-00-00";
        this.date_updated="2000-00-00"; 
        this.date_deleted="2000-00-00";
    }

}