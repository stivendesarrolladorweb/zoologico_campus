import 'reflect-metadata';
import { Router } from 'express';
import  dtoHistorial from "../controllerDTO/historial_medico.js";
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

const appDTODataHistorial= Router();

appDTODataHistorial.use( async(req, res, next)=>{
    try{
        let data = plainToClass(dtoHistorial, req.body);
        await validate(data);
        req.body = JSON.parse(JSON.stringify(data));
        req.data=undefined;
        next();
    }catch(error){
        res.status(error.status).send({status:400, message:error.message})
    }
});
export{
    appDTODataHistorial    
} 