import passport from "../passport/passport.js";

const validateToken = async (req, res, next) => {
    passport.authenticate("bearer", {session: false}, (err, decoded) => {
        if(!decoded) return res.status(403).send({status: 401, message: "Autenticacion invalida"})
        else {
            let permissions = decoded.toLowerCase(); 
            let method = (req.method).toLowerCase(); 
            if(method==permissions){
                return next(); 
            }else{
                return res.status(403).send({status: 403, message: `Lo sentimos, usted no tiene permisos para realizar esta operacion de tipo ${method}`})
            }           
        }
    })(req, res, next)
}

export {
    validateToken
}