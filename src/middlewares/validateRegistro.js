import "reflect-metadata";
import {plainToClass} from "class-transformer";
import express from "express";
import {dtoRegistro} from "../controllerDTO/registro.js";
import {validate} from "class-validator"

const validateRegistro = express();

validateRegistro.use(async(req,res, next)=>{
    try{
        let data = plainToClass(dtoRegistro, req.body, {excludeExtraneousValues:true})
        await validate(data)
        req.body=data;
        next();
    }catch(error){
        res.status(error.status).send(error.message)
    }
})

export default validateRegistro;

