const validateId = (id) =>{
    let result = "";
    if(!isNaN(id)){
        if(id>0){
            result=true;
        }else{
            result="El id debe ser un numero positivo"
        }
    }else{
        result="El parametro debe ser un int." 
    }
    return result
}

export {validateId}; 