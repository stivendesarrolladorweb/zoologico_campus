import 'reflect-metadata';
import { Router } from 'express';
import  dtoVeterinario  from "../controllerDTO/veterinario.js";
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

const appDTODataVeterinario= Router();

appDTODataVeterinario.use( async(req, res, next)=>{
    try{
        let data = plainToClass(dtoVeterinario, req.body);
        await validate(data);
        req.body = JSON.parse(JSON.stringify(data));
        req.data=undefined;
        next();
    }catch(error){
        res.status(error.status).send({status:400, message:error.message})
    }
});
export{
    appDTODataVeterinario    
} 