import 'reflect-metadata';
import { Router } from 'express';
import { dtoAnimal } from "../controllerDTO/animal.js";
import { plainToClass } from 'class-transformer';
import { validate } from 'class-validator';

const appDTODataAnimal= Router();

appDTODataAnimal.use( async(req, res, next)=>{
    try{
        let data = plainToClass(dtoAnimal, req.body, {excludeExtraneousValues:true});
        await validate(data);
        req.body =data;
        next();
    }catch(error){
        res.status(error.status).send({status:400, message:error.message})
    }
});
export{
    appDTODataAnimal    
} 