import express from 'express';
import passport from "passport";
import dotenv from 'dotenv';
import appRegistro from './routers/registro.js';
import appVeterinario from './routers/veterinario.js';
import appAnimal from './routers/animal.js';
import appLogin from './routers/login.js';
import appHistorial from './routers/historial.js';

dotenv.config();
let appExpress = express();
appExpress.use(express.json());
appExpress.use(passport.initialize());

appExpress.use(appRegistro);
appExpress.use(appVeterinario);
appExpress.use(appAnimal);
appExpress.use(appLogin);
appExpress.use(appHistorial);

appExpress.use("/",(req,res)=>{
    res.status(404).json({status:"404",message:"Ruta no encontrada"})
})

let config = JSON.parse(process.env.MY_SERVER)
appExpress.listen(config, ()=>{
    console.log(`http://${config.hostname}:${config.port}`)
})

